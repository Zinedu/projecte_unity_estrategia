﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NPCLogic : MonoBehaviour
{

    public static int difficultyModifier = 5;

    int saved = 0;
    int lost = 0;

    GameObject teleporter1;
    GameObject teleporter2;

    GameObject villain;

    GameObject textRescatats;
    GameObject textPerduts;

    public static int finalscore = 0;
    public GameObject[] npcArray = new GameObject[5];
    public int[] teleporterArray = new int[5];
    public int[] selectorArray = new int[5];
    public int actual = 0;
    public Vector3 spawnerVector = new Vector3(0, 0, -1);
    public float currentSpeed = 0.1f;
    public int speedUpTimer = 1500;
    public int speedTimer = 0;
    public int dangerSelection = 0;
    public static GameObject currentTarget;
    int minionSpawnTimerTotal = 500;
    int minionSpawnTimer = 0;
    public static int bullets = 2;
    int bulletTimer = 600;
    int bulletTimerAct = 0;
    GameObject uiBullet1;
    GameObject uiBullet2;
    // Start is called before the first frame update

    private void OnEnable()
    {
        //Subscribirse al evento del teleporter

        Teleporter.OnContact += Teleport;
        NPCdeath.OnDeath += Death;
        MinionLogic.OnMinionDeath += MinionDeath;
    }

    void Start()
    {
        currentTarget = null;
        textRescatats = GameObject.Find("Rescatats");
        textPerduts = GameObject.Find("Perduts");

        for(int a = 0; a < selectorArray.Length;a++)
        {
            selectorArray[a] = a;
        }

        //Mezclar el orden de los NPCs

        for(int b = 0; b < 20; b++)
        {
            for(int ab = 0; ab < selectorArray.Length; ab++)
            {
                int temp = selectorArray[ab];
                int target = Random.Range(0, selectorArray.Length);
                int temp2 = selectorArray[target];

                temp2 = selectorArray[target];

                selectorArray[ab] = temp2;
                selectorArray[target] = temp;

            }

            uiBullet1 = GameObject.Find("bullet1_ui");
            uiBullet2 = GameObject.Find("bullet2_ui");
            
        }

        villain = Instantiate(Resources.Load<GameObject>("NPCs/Villain"), new Vector3(0, 0, -1), Quaternion.identity);

        teleporter1 = GameObject.Find("Teleporter 1");
        teleporter2 = GameObject.Find("Teleporter 2");

        npcArray[0] = Resources.Load<GameObject>("NPCs/Villager_A");
        npcArray[1] = Resources.Load<GameObject>("NPCs/Villager_B Variant");
        npcArray[2] = Resources.Load<GameObject>("NPCs/Villager_C Variant");
        npcArray[3] = Resources.Load<GameObject>("NPCs/Villager_D Variant");
        npcArray[4] = Resources.Load<GameObject>("NPCs/Villager_E Variant");

        spawnerVector = new Vector3(Random.Range(5, 7), 9, -1);
        npcArray[0] = Instantiate<GameObject>(npcArray[0], spawnerVector, Quaternion.identity);

        spawnerVector = new Vector3(Random.Range(-5, -2), 9, -1);
        npcArray[1] = Instantiate<GameObject>(npcArray[1], spawnerVector, Quaternion.identity);

        spawnerVector = new Vector3(Random.Range(-8, -6), -9, -1);
        npcArray[2] = Instantiate<GameObject>(npcArray[2], spawnerVector, Quaternion.identity);

        spawnerVector = new Vector3(Random.Range(-3, 3), -9, -1);
        npcArray[3] = Instantiate<GameObject>(npcArray[3], spawnerVector, Quaternion.identity);

        spawnerVector = new Vector3(Random.Range(6, 8), -9, -1);
        npcArray[4] = Instantiate<GameObject>(npcArray[4], spawnerVector, Quaternion.identity);
        int i = 0;
        foreach(GameObject g in npcArray)
        {
            Vector2 tmpPos = new Vector2(g.GetComponent<Transform>().position.x, g.GetComponent<Transform>().position.y);
            Vector2 posTl1 = new Vector2(teleporter1.GetComponent<Transform>().position.x, teleporter1.GetComponent<Transform>().position.y);
            Vector2 posTl2 = new Vector2(teleporter2.GetComponent<Transform>().position.x, teleporter2.GetComponent<Transform>().position.y);
            if (Vector2.Distance(posTl1, tmpPos) >= Vector2.Distance(posTl2, tmpPos))
            {
                teleporterArray[i] = 2;
            }
            else
            {
                teleporterArray[i] = 1;
            }
            i++;
        }

        dangerSelection = selectorArray[actual];
    }

    // Update is called once per frame
    void Update()
    {
        //Actualiza el gameobject que referencia el objectivo actual para usarlo en otros scripts
        if(currentTarget != npcArray[dangerSelection])
        {
            currentTarget = npcArray[dangerSelection];
        }
        switch (teleporterArray[dangerSelection]) {
            case 1:
                
                //x

                if(npcArray[dangerSelection].GetComponent<Transform>().position.x == teleporter1.GetComponent<Transform>().position.x)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(0, npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.y);
                }

                if (npcArray[dangerSelection].GetComponent<Transform>().position.x < teleporter1.GetComponent<Transform>().position.x)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(0.5f, npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.y);
                }

                if (npcArray[dangerSelection].GetComponent<Transform>().position.x > teleporter1.GetComponent<Transform>().position.x)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(-0.5f, npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.y);
                }

                //y

                if (npcArray[dangerSelection].GetComponent<Transform>().position.y == teleporter1.GetComponent<Transform>().position.y)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.x, 0);
                }

                if (npcArray[dangerSelection].GetComponent<Transform>().position.y < teleporter1.GetComponent<Transform>().position.y)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.x, 0.5f);
                }

                if (npcArray[dangerSelection].GetComponent<Transform>().position.y > teleporter1.GetComponent<Transform>().position.y)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.x, -0.5f);
                }

                break;
            case 2:

                //x

                if (npcArray[dangerSelection].GetComponent<Transform>().position.x == teleporter2.GetComponent<Transform>().position.x)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(0, npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.y);
                }

                if (npcArray[dangerSelection].GetComponent<Transform>().position.x < teleporter2.GetComponent<Transform>().position.x)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(0.5f, npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.y);
                }

                if (npcArray[dangerSelection].GetComponent<Transform>().position.x > teleporter2.GetComponent<Transform>().position.x)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(-0.5f, npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.y);
                }

                //y

                if (npcArray[dangerSelection].GetComponent<Transform>().position.y == teleporter2.GetComponent<Transform>().position.y)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.x, 0);
                }

                if (npcArray[dangerSelection].GetComponent<Transform>().position.y < teleporter2.GetComponent<Transform>().position.y)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.x, 0.5f);
                }

                if (npcArray[dangerSelection].GetComponent<Transform>().position.y > teleporter2.GetComponent<Transform>().position.y)
                {
                    npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity = new Vector2(npcArray[dangerSelection].GetComponent<Rigidbody2D>().velocity.x, -0.5f);
                }

                break;
             
        }

        //enemigo persiguiendo al NPC activo

        //x
        if (saved + lost != npcArray.Length)
        {
            if (npcArray[dangerSelection].GetComponent<Transform>().position.x > villain.GetComponent<Transform>().position.x)
            {
                villain.GetComponent<Rigidbody2D>().velocity = new Vector2(currentSpeed - 0.8f, villain.GetComponent<Rigidbody2D>().velocity.y);
            }
            if (npcArray[dangerSelection].GetComponent<Transform>().position.x == villain.GetComponent<Transform>().position.x)
            {
                villain.GetComponent<Rigidbody2D>().velocity = new Vector2(0, villain.GetComponent<Rigidbody2D>().velocity.y);
            }

            if (npcArray[dangerSelection].GetComponent<Transform>().position.x < villain.GetComponent<Transform>().position.x)
            {
                villain.GetComponent<Rigidbody2D>().velocity = new Vector2(-currentSpeed + 0.8f, villain.GetComponent<Rigidbody2D>().velocity.y);
            }

            //y
            if (npcArray[dangerSelection].GetComponent<Transform>().position.y > villain.GetComponent<Transform>().position.y)
            {
                villain.GetComponent<Rigidbody2D>().velocity = new Vector2(villain.GetComponent<Rigidbody2D>().velocity.x, currentSpeed - 0.8f);
            }
            if (npcArray[dangerSelection].GetComponent<Transform>().position.y == villain.GetComponent<Transform>().position.y)
            {
                villain.GetComponent<Rigidbody2D>().velocity = new Vector2(villain.GetComponent<Rigidbody2D>().velocity.x, 0);
            }

            if (npcArray[dangerSelection].GetComponent<Transform>().position.y < villain.GetComponent<Transform>().position.y)
            {
                villain.GetComponent<Rigidbody2D>().velocity = new Vector2(villain.GetComponent<Rigidbody2D>().velocity.x, -currentSpeed + 0.8f);
            }
        }
        else
        {
            villain.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
        textPerduts.GetComponent<Text>().text = "Perduts: " + lost + "/" + npcArray.Length;
        textRescatats.GetComponent<Text>().text = "Rescatats: " + saved + "/" + npcArray.Length;

        speedTimer++;
        if(speedTimer == speedUpTimer)
        {
            currentSpeed = currentSpeed + 0.01f;
            speedTimer = 0;
        } 

        minionSpawnTimer++;
        if(minionSpawnTimer == minionSpawnTimerTotal)
        {
            Instantiate(Resources.Load("NPCs/Minion"), new Vector3(villain.GetComponent<Transform>().position.x, villain.GetComponent<Transform>().position.y + Random.Range(0, 2) + (villain.GetComponent<SpriteRenderer>().size.y * villain.GetComponent<Transform>().localScale.y) / 2, -1), Quaternion.identity);
            Instantiate(Resources.Load("NPCs/Minion"), new Vector3(villain.GetComponent<Transform>().position.x, villain.GetComponent<Transform>().position.y - Random.Range(0, 2) - (villain.GetComponent<SpriteRenderer>().size.y * villain.GetComponent<Transform>().localScale.y) / 2, -1), Quaternion.identity);
            minionSpawnTimer = 0;
        }

        bulletTimerAct++;
        if(bulletTimerAct == bulletTimer)
        {
            if(bullets < 2)
            {
                bullets++;
            }
            bulletTimerAct = 0;
        }

        switch (bullets)
        {
            case 0:
                uiBullet1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Others/obstacle_ui_gray");
                uiBullet2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Others/obstacle_ui_gray");
                break;
            case 1:
                uiBullet1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Others/obstacle_ui_green");
                uiBullet2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Others/obstacle_ui_gray");
                break;
            case 2:
                uiBullet1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Others/obstacle_ui_green");
                uiBullet2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Others/obstacle_ui_green");
                break;
        }

    }

    void Teleport()
    {

        //cambia de objetivo

        if (actual < selectorArray.Length -1)
        {
            actual++;
            dangerSelection = selectorArray[actual];

        }
        saved++;
        finalscore++;
        if (saved + lost == npcArray.Length && SceneManager.GetActiveScene().name != "Level2")
        {
            SceneManager.LoadScene("Level2");
        }
        else
        {
            if (SceneManager.GetActiveScene().name == "Level2" && saved + lost == npcArray.Length)
            {
                SceneManager.LoadScene("Game_over"); 
            }
        }


    }

    void Death()
    {
        if (actual < selectorArray.Length - 1)
        {
            actual++;
            dangerSelection = selectorArray[actual];
            
        }
        lost++;
        if (saved + lost == npcArray.Length && SceneManager.GetActiveScene().name != "Level2")
        {
            SceneManager.LoadScene("Level2");
        }
        else
        {
            if (SceneManager.GetActiveScene().name == "Level2" && saved + lost == npcArray.Length)
            {
                SceneManager.LoadScene("Game_over");
            }
        }
    }

    void MinionDeath()
    {
        if (actual < selectorArray.Length - 1)
        {
            actual++;
            dangerSelection = selectorArray[actual];

        }
        lost++;
        if (saved + lost == npcArray.Length && SceneManager.GetActiveScene().name != "Level2")
        {
            SceneManager.LoadScene("Level2");
        }
        else
        {
            if (SceneManager.GetActiveScene().name == "Level2" && saved + lost == npcArray.Length)
            {
                SceneManager.LoadScene("Game_over");
            }
        }
    }

}
