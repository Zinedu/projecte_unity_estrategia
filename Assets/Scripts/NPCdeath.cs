﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCdeath : MonoBehaviour
{
    public delegate void death();
    public static event death OnDeath;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Villager")
        {
            if (OnDeath != null)
            {
                //Teletransporta el NPC fuera de la vision del jugador
                collision.gameObject.GetComponent<Transform>().position = new Vector2(-200, -200);
                OnDeath();
            }
        }
    }
}
