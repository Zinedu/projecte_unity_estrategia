﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionLogic : MonoBehaviour
{

    float currentSpeed = 0.2f;
    int timerSpeed = 0;
    int timerIncrease = 500;
    int randomDirection = 0;

    public delegate void minionDeath();
    public static event minionDeath OnMinionDeath;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Timers de acceleracion
        timerSpeed++;
        if(timerSpeed == timerIncrease)
        {
            currentSpeed += 0.2f;
            randomDirection = Random.Range(0, 5);

        }

        if(this.GetComponent<Transform>().position.x > 8)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-10, 0));
        }
        if(this.GetComponent<Transform>().position.x < -8)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(10, 0));
        }
        if(this.GetComponent<Transform>().position.y > 5)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -10));
        }
        if (this.GetComponent<Transform>().position.y < -5)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10));
        }

        switch (randomDirection)
        {
            case 0:
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(currentSpeed, currentSpeed);
                break;
            case 1:
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-currentSpeed, currentSpeed);
                break;
            case 2:
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(currentSpeed, -currentSpeed);
                break;
            case 3:
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-currentSpeed, -currentSpeed);
                break;
            case 4:
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag != "Villager" && collision.gameObject.tag !=  "Obstacle")
        {
            Physics2D.IgnoreCollision(this.gameObject.GetComponent<Collider2D>(), collision.gameObject.GetComponent<Collider2D>());
        }
        else
        {
            switch (collision.gameObject.tag)
            {
                case "Villager":
                    collision.gameObject.GetComponent<Transform>().position = new Vector2(-200, -200);
                    if(OnMinionDeath != null)
                    {
                        OnMinionDeath();
                    }
                    break;
                case "Obstacle":
                    GameObject.Destroy(this);
                    break;
            }
        }
    }

    private void OnMouseDown()
    {
        if(NPCLogic.bullets > 0)
        {
            NPCLogic.bullets--;
            GameObject.Destroy(this.gameObject);
        }
    }
}
