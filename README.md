**INTRO**

Hem fet un projecte d'estrategia on l'objectiu és protegir NPCs fins que arribin al seu destí utilitzant un obstacle mòbil per bloquejar a un enemic i eliminant a enemics mes petits fent clic en ells.

El joc disposa de dos nivells i una pantalla de Game Over, la dificultat també incrementa a mesura que el nivell avança.

El script principal és NPCLogic, que controla el funcionament dels NPCs, els enemics, i la puntuació, mentre que els delegates de col·lisions estan en NPCCollider, NPCdeath, Teleporter i MinionLogic on s'utilitzen per detectar NPCs morts o salvats.

El joc no disposa de pathfinding a part d'anar en la direcció general del NPC seleccionat com a target, per tant no es disposa d'altres obstacles a part dels que l'usuari te per controlar.

La UI disposa d'elements visuals per indicar el número de minions (Els enemics petits amb un sol ull) que el jugador pot eliminar, el jugador pot acumular un màxim de dos i es recarreguen amb el temps.

**FUNCIONAMENT**

Els npcs estan disposats en un array de gameobjects que s'aleatoritza abans de començar per fer que el seu aspecte no sigui sempre el mateix després s'utilitza un int de target que indica la posició en el array per tant d'elegir quin NPC es ha de perseguir.

Els NPCs fan spawn just al borde del mapa, fora la càmera en una X aleatòria i aniran cap al teleporter més proper.

Els NPCs morts i rescatats no és destrueixen en cas de ser salvats o morts, simplement és teletransporten fora del mapa i són ignorats per tal d'evitar problemes en el array.

En cas que un NPC fes contacte amb un teletransportador, l'enemic o un minion se'l teletransporta fora del mapa a les coordenades 200,200 o -200,-200 i es dispara un delegate amb el event corresponent, alterant la puntuació en NPCLogic.

La puntuació es desa en una variable estàtica perquè no és reinici.

El jugador disposa de dos "bales" per intentar obrir camí als NPCs, el jugador recupera una bala cada 600 frames, el número de bales és indicat en la UI amb dos quadrats.

L'obstacle principal és un quadrat blau que es pot moure amb el ratolí, el quadrat és capaç d' empenyer a l'enemic mentre s'aguanta amb el ratolí però pot ser empès si no s'aguanta, l'obstacle es incapaç d'empènyer els minions, i l'única forma d'interactuar amb ells és eliminant-los fent gastar una bala.

**PROBLEMES I ERRORS**

- Errors:

- - Si l'usuari treu l'obstacle del mapa no es pot recuperar.
- - Si un dels minions o l'enemic fa contacte amb un NPC que no és el seu target, començarà a perseguir a l'objectiu incorrecte.
- - El timer per recuperar la bala no detecta si el jugador ja té dues bales abans de començar, per tant és possible recuperar una bala quasi a l'instant si coincideix amb el timer.



