﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{

    public delegate void Contact();
    public static event Contact OnContact;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Villager")
        {
            if (OnContact != null)
            {
                //Teletransporta el NPC fuera de la vision del jugador
                collision.gameObject.GetComponent<Transform>().position = new Vector2(200, 200);
                OnContact();
            }
        }
        else
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), this.GetComponent<Collider2D>());
        }
    }

}
