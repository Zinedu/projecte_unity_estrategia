﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

public class ObstacleControl : MonoBehaviour
{
    private Vector3 mousePosition;
    private Rigidbody2D rb;
    private Vector2 direction;
    public float speed = 40f;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDrag()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //Diferencia la distancia entre el mouse y el rigidbody y lo convierte en velocity (multiplicado por el speed que le has asignado)
        direction = (mousePosition - transform.position).normalized;
        rb.velocity = new Vector2(direction.x * speed, direction.y * speed);
    }

    private void stayinside()
    {
        //Mathf clamp lo que hace es que si el valor supera al minimo, el valor se queda en el valor minimo sin superarlo, y si supera el maximo, el vador se queda en el valor maximo sin superarlo.
        //Nota, tienes que calcular la distancia de la ventana
        //Square
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -8.25f, 8.25f), Mathf.Clamp(transform.position.y, -4.5f, 4.5f), transform.position.z);
    }

    private void limit()
    {


        //Eje x


        if (this.GetComponent<Rigidbody2D>().velocity.x < speed && this.GetComponent<Rigidbody2D>().velocity.x > -speed)
        {
            //controls();

        }
        else
        {
            if (this.GetComponent<Rigidbody2D>().velocity.x > speed)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else if (this.GetComponent<Rigidbody2D>().velocity.x < -speed)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, this.GetComponent<Rigidbody2D>().velocity.y);
            }
        }


        //Eje y


        if (this.GetComponent<Rigidbody2D>().velocity.y < speed && this.GetComponent<Rigidbody2D>().velocity.y > -speed)
        {
            //controls();
        }
        else
        {
            if (this.GetComponent<Rigidbody2D>().velocity.y > speed)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, speed);
            }
            else if (this.GetComponent<Rigidbody2D>().velocity.y < -speed)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -speed);
            }
        }
    }

    private void controls()
    {

        //Eje x

        if (this.GetComponent<Rigidbody2D>().velocity.x == 0f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (this.GetComponent<Rigidbody2D>().velocity.x > 0f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x - 0.1f, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (this.GetComponent<Rigidbody2D>().velocity.x < 0f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x + 0.1f, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        if (this.GetComponent<Rigidbody2D>().velocity.x < 0.1f && this.GetComponent<Rigidbody2D>().velocity.x > -0.1f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, this.GetComponent<Rigidbody2D>().velocity.y);
        }


        //Eje y

        if (this.GetComponent<Rigidbody2D>().rotation != 0f)
        {
            //print(this.GetComponent<Rigidbody2D>().rotation);
        }
        if (this.GetComponent<Rigidbody2D>().velocity.y == 0f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        }
        else if (this.GetComponent<Rigidbody2D>().velocity.y > 0f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, this.GetComponent<Rigidbody2D>().velocity.y - 0.3f);
        }
        else if (this.GetComponent<Rigidbody2D>().velocity.y < 0f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, this.GetComponent<Rigidbody2D>().velocity.y + 0.3f);
        }
        if (this.GetComponent<Rigidbody2D>().velocity.y < 0.3f && this.GetComponent<Rigidbody2D>().velocity.y > -0.3f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0f);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Villager")
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), this.GetComponent<Collider2D>());
        }

        if(collision.gameObject.tag == "Villain")
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }

    private void OnMouseUp()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }


}
